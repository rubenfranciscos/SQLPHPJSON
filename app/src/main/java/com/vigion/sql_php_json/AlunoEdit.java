package com.vigion.sql_php_json;;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AlunoEdit extends AppCompatActivity {

    //Objetos para controlo dos objetos graficos
    private TextView textViewNproc;
    private EditText editTextNome;
    private EditText editTextUserName;
    private EditText editTextPassword;

    private String nProc; // recebe valor da AlunoListView

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aluno_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fabDelete = (FloatingActionButton) findViewById(R.id.fabAlunoDelete);
        fabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Registo em EliminaÃ§Ã£o", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                confirmDeleteAluno();
            }
        });

        FloatingActionButton fabUpdate = (FloatingActionButton) findViewById(R.id.fabAlunoUpdate);
        fabUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Registo em AtualizaÃ§Ã£o", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                updateAluno();
            }
        });

        FloatingActionButton fabAlunoList = (FloatingActionButton) findViewById(R.id.fabAlunoList);
        fabAlunoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "List de Aluno", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });




        //ExtraÃ§Ã£o do atributo passado pela activity pela activity da ListView
        Intent intent = getIntent();
        nProc = intent.getStringExtra(Config.ALUNO_NPROC);


        //aquisiÃ§Ã£o do controlo dos objetos graficos
        textViewNproc = (TextView) findViewById(R.id.textViewNproc);
        editTextNome = (EditText) findViewById(R.id.editTextNome);
        editTextUserName = (EditText)findViewById(R.id.editTextUserName);
        editTextPassword = (EditText)findViewById(R.id.editTextPassword);

        textViewNproc.setText(textViewNproc.getText() + nProc); //atualiza a textViewNproc com nProc
        Log.i("LOG:", "alunoEdit - onCreate(): Vou executar getAluno() com nProc: " + nProc);
        getAluno(); //Obtem da Base de Dados, o aluno, cujo nProc foi passado no Intent
    }

    private void getAluno() {
        class GetAluno extends AsyncTask<Void, Void, String> {
            ProgressDialog loading; //cria objeto ProgressBar

            @Override
            protected String doInBackground(Void... params) {
                WebConnection rh = new WebConnection();
                Log.i("LOG","AlunoEdit - GETAluno() - doInBackground() - WebConnection.sendGetRequestParam() URL: " + Config.URL_GET_ALL);
                String resposta = rh.sendGetRequest(Config.URL_GET_ALUNO, nProc);
                Log.i("LOG","AlunoEdit - GETAluno() - doInBackground() - WebConnection.sendGetRequestParam() Resposta: " + resposta);
                return resposta;
            }

            @Override
            protected void onPreExecute() {//inicializa a progressBar
                loading = ProgressDialog.show(AlunoEdit.this, "A processar receÃ§Ã£o de dados...","SÃ³ um momento...",false,false);
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String resposta) {
                Log.i("LOG", "Chegou a getAluno - onPostExecute");
                loading.dismiss(); //termina a progressBar
                showAluno(resposta);//chama o mÃ©todo para mostrar o registo no layout
                //super.onPostExecute(resposta);
            }
        }
        GetAluno ga = new GetAluno();
        ga.execute();
    }

    private void showAluno(String resposta) {
        try {
            Log.i("LOG:","AlunoEdit - showAluno() - JSON: " + resposta);

            //Objeto JSON para tratamento dos dados json recebidos na string do paramtro
            JSONObject alunoJsonObject = new JSONObject(resposta); //Converte a string Json
            Log.i("LOG:","AlunoEdit - showAluno() - JSONObject alunoJsonObject " + alunoJsonObject);

            JSONArray result = alunoJsonObject.getJSONArray(Config.TAG_JSON_ARRAY); //separa os elementos JSON, num array
            Log.i("LOG:", "AlunoEdit - showAluno() - result: " + result);

            JSONObject aluno  = result.getJSONObject(0); //Extrai o 1Âº elemento json do array: O aluno
            Log.i("LOG:", "AlunoEdit - showAluno() - JSON Object aluno: " + aluno);

            String nome = aluno.getString(Config.TAG_NOME);
            String userName = aluno.getString(Config.TAG_USERNAME);
            String password = aluno.getString(Config.TAG_PASSWORD);

            //extraÃ§Ã£o dos dados do objecto JSON
            editTextNome.setText(nome);
            editTextUserName.setText(userName);
            editTextPassword.setText(password);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //SQL update, executda a partir do botÃ£o da activity
    private void updateAluno() {
        //recolha dos dados das EditText
        final String nome =  editTextNome.getText().toString().trim();
        final String userName = editTextUserName.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        //validaÃ§Ã£o de dados (BD Ã© not null para estes campos)
        if (nome.isEmpty() || userName.isEmpty() || password.isEmpty()){
            Toast.makeText(AlunoEdit.this, "Campos sem dados", Toast.LENGTH_LONG).show();
        }
        else
        {
            //classe assincrona para comunicaÃ§Ã£o em Thread a executador em Background
            class UpdateEmployee extends AsyncTask<Void,Void,String>{

                ProgressDialog loading;

                @Override
                protected String doInBackground(Void... params) {
                    HashMap<String, String> haspMap = new HashMap<>();
                    haspMap.put(Config.KEY_NPROC, nProc);
                    haspMap.put(Config.KEY_NOME, nome);
                    haspMap.put(Config.KEY_USERNAME, userName);
                    haspMap.put(Config.KEY_PASSWORD, password);

                    WebConnection rh = new WebConnection();
                    String resposta = rh.sendPostRequest(Config.URL_UPDATE_ALUNO, haspMap);

                    return resposta;
                }

                @Override
                protected void onPreExecute() {

                    super.onPreExecute();

                    loading = ProgressDialog.show(AlunoEdit.this,"A atualizar...","Esperar...",false,false);
                }

                @Override
                protected void onPostExecute(String resposta) {
                    super.onPostExecute(resposta);

                    loading.dismiss();;
                    Toast.makeText(AlunoEdit.this, resposta, Toast.LENGTH_LONG).show();
                    startActivity(new Intent(AlunoEdit.this, AlunoListview.class));
                }
            }
            //criaÃ§Ã£o do objeto assincrono e execuÃ§Ã£o
            UpdateEmployee update = new UpdateEmployee();
            update.execute();
        }
    }

    //SQL delete, executada a partir do botÃ£o Activity
    private void deleteAluno(){

        //class assincrona para comunicaÃ§Ã£o em Thread a exectuar em Background
        class DeleteEmployee extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;

            @Override
            protected String doInBackground(Void... params) {

                WebConnection rh = new WebConnection();
                String resposta = rh.sendGetRequest(Config.URL_DELETE_ALUNO, nProc);

                return resposta;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(AlunoEdit.this,"A apagar...","Esperar...",false,false);

            }

            @Override
            protected void onPostExecute(String resposta) {
                super.onPostExecute(resposta);
                loading.dismiss();
                Toast.makeText(AlunoEdit.this, resposta, Toast.LENGTH_LONG).show();
                startActivity(new Intent(AlunoEdit.this, AlunoListview.class));
            }
        }

        DeleteEmployee de = new DeleteEmployee();
        de.execute();
    }

    private void confirmDeleteAluno(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Confirma a eliminaÃ§Ã£o");

        alertDialogBuilder.setPositiveButton("Sim",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAluno();
                        startActivity(new Intent(AlunoEdit.this, AlunoListview.class));
                    }
                });

        alertDialogBuilder.setNegativeButton("NÃ£o",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //faz nada
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
