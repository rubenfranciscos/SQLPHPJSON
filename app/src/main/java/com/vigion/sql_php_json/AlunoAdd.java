package com.vigion.sql_php_json;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

public class AlunoAdd extends AppCompatActivity {

    private EditText editTextNome;
    private EditText editTextUserName;
    private EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aluno_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextNome = (EditText) findViewById(R.id.editTextNome);
        editTextUserName = (EditText) findViewById(R.id.editTextUserName);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);


        //-----------------------------------------------------------------------------------------------------
        FloatingActionButton fabAlunoLista = (FloatingActionButton) findViewById(R.id.fabAlunoList);
        fabAlunoLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(AlunoAdd.this, AlunoListview.class));
                //LIST VIEW
            }
        });
        //-----------------------------------------------------------------------------------------------------

        FloatingActionButton fabAdd = (FloatingActionButton) findViewById(R.id.fabAlunoAdd);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAluno();
                startActivity(new Intent(AlunoAdd.this, AlunoListview.class));
                //ADD ALUNO
            }
        });
        Log.i("LOG: ", "MAIN ACTIVITY - onCreate() - Completa");
        //-----------------------------------------------------------------------------------------------------

    }
    //ADICIONA UM aLUNO
    public void addAluno(){
        //Recolha dos dados inseridos pelo user
        final String nome = editTextNome.getText().toString().trim();
        final String username = editTextUserName.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        if(nome.isEmpty() || username.isEmpty() || password.isEmpty()){
            Toast.makeText(AlunoAdd.this , "Campos sem dados", Toast.LENGTH_LONG).show();
        }
        else {
            class AddAluno extends AsyncTask<Void, Void, String> {
                ProgressDialog loading;     //ProgressBar em Caixa de Mensagem

                @Override
                protected void onPreExecute() {
                    loading = ProgressDialog.show(AlunoAdd.this, "A processar envio de dados...", "So um momento..", false, false);
                }

                @Override
                protected String doInBackground(Void... params) {
                    //Cria uma colectionvalue pair, de 2 strings: nomes de ccampos e os dados das EditText
                    HashMap<String, String> dados = new HashMap<>();
                    dados.put(Config.KEY_NOME, nome);
                    dados.put(Config.KEY_USERNAME, username);
                    dados.put(Config.KEY_PASSWORD, password);

                    //Comunicaçao dos dados para insert usando o metodo sensPostRRequest() da class WebConnection
                    WebConnection wc = new WebConnection();         //Cria objeto WebConnection
                    Log.i("LOG", "AlunoAdd - Class AddAluno - doInBackground - Vou Chamar WebConnection.sendPostRequest() com url: " + Config.URL_ADD);
                    String resp = wc.sendPostRequest(Config.URL_ADD, dados);     //Envia dados e espera a resposta
                    Log.i("LOG:", "AlunoAdd - Class ADDAluno - doInBackgroud\n" + resp);
                    return resp;        //Devolve a resposta
                }


                @Override
                protected void onPostExecute(String response) {
                    loading.dismiss();
                    Toast.makeText(AlunoAdd.this, response, Toast.LENGTH_LONG).show();
                    //startActivity((new Intent(AddAluno.this, AlunoListView.class)));
                }
            }
            AddAluno newAluno = new AddAluno();         //Cria um novo objeto assincrono
            newAluno.execute();                         //Executa o objeto
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
