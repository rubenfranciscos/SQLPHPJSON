package com.vigion.sql_php_json;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruben on 19/01/2016.
 */
public class WebConnection {
    //SendPostRequest()
    //Executa script php online com insert, update ou deelte: método POST
    //Envia dados e o nome do script a executar
    //O server recebe a comunicação com o carimbo POST, extrai os dados e insere-os no script
    //em variaveis definidas para esse efeito
    //Executa o script (insert, update ou delete) e devolve uma respodsta: uma string tb do script.
    //HttpUrlConnection recebe a proposta que, depois da interpretada e convertida, é mostrada.
    //
    //O método tem dois parametros:
    //1º - URL do script php, para onde são enviados os dados (POST REQUEST) Pedido de Entrega
    //2º - Collection HasMap (ValuePair) com os dados a enviar com o PostRequest
    public String sendPostRequest(String phpURL, HashMap<String,String> postData){
        URL url;    //Objeto URL para formatar o endereço a enviar

        Log.i("LOG: ", "WebConnection - URL: " + phpURL);

        //StringBuilder para tratar a resposta do servidor php
        StringBuilder sb = new StringBuilder();

        try{
            url = new URL(phpURL);      //Inicializa com o URL recebido por parametro

            //Cria uma ligaçao
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            //Configuraçao da ligacao

            conn.setConnectTimeout(60000);          //Limite de tempo para fazaer a ligação
            conn.setReadTimeout(60000);             //Limite de tempo para receber resposta do server
            conn.setRequestMethod("POST");          //Tipo de Ligação: Post- Entrega de dados a um script
            conn.setDoInput(true);                  //Sentido de dados que vai ser processado
            conn.setDoOutput(true);                 //Neste caso: Saida para Post e Entrada com a resposta

            OutputStream os = conn.getOutputStream();       //Cria um Canal de saida

            //Cria um cais de saida para escrita dos dados e liga-o ao canal de saida
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            //Escrita dos dados no cais de saida
            //É necessario convert los numa string com o formato url, para simular o Browser
            writer.write(convertData2Url(postData));

            writer.flush();     //Comunicaçao executada
            writer.close();     //Fecho do cais de saidaw
            os.close();         //Fecho do canal de saida

            //Resposta
            int responseCode = conn.getResponseCode();          //Obtem code com resposta do server PHP
            if(responseCode == HttpURLConnection.HTTP_OK)       //se estiver ok
            {
                //Abre Cais e Canal de entrada para receber resposta Completa do Server PHP
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                sb = new StringBuilder();                       //Inicializacao da StringBuilder
                String response;                                //String para ler linha a linha


                //Leiturta, linha, da resposta do server, para um StringBuilder
                while ((response = br.readLine()) != null){
                    sb.append(response);
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("Log:", "WebConnection - sendPostRequest - Execuçao: " + e.getMessage());
        }
        return sb.toString();           //Devolve a StringBuilder com a resposta do server
    }

    //--------------------------------------------------------------------------------------
    //sendGetRequest() tem 2 variantes:
    // - 1 parametro, obtem queries de todos os registos: 1º parametro é para o script php
    // - 2 parametors, obtem queries de todos os registos: o 2º para o id
    //--------------------------------------------------------------------------------------

    // sentGetRequest(1 parametro)
    // Executa script php online com query para obter registos da BD: Método GET
    // Envia dados e o nome do script a executar, com o carimbo GET
    // O server recebe a comunicação, extrai os dados e insere-os no script, em variáveis próprias
    // Executa o script com a query, agora com dados passados e devolve o resultado da query ou erro.
    // HttpUrlConnection recebe a resposta, com os registos, através de con.getInputStream:
    //  - pelo canal de entrada inPutStreamReader
    //  - no cais da entrada BuferedRader
    // Os dados são empilhos na StringBuilder sb e devolvida para quem chamou o metodo
    //
    // O metodo tem 1 parametro: URL do script php;

    public String sendGetRequest(String phpURL){
        StringBuilder sb = new StringBuilder();
        try{
            URL url = new URL(phpURL);              //Converte a string com o url do script PHP
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            //Conversao da resposta do server, com os dados, para uma stringBuilder
            //lembrar que vem numa string, com formato de um array
            String strLine;

            while ((strLine = bufferedReader.readLine()) != null){
                sb.append(strLine+"\n");            //Leitura, linha a linha,da resposta do server
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            Log.i("LOG:", "WebConnection - sendGetRequest - Execucao: " + e.getMessage());
        }
        return sb.toString();                   //Devolve a StringBuilder com os dados
    }
    // sentGetRequest(1 parametro)
    // Executa script php online com query para obter um so registo indentificado por ID: Método GET
    // Envia dados e o nome do script a executar
    // O server recebe a comunicação, extrai os dados e insere-os no script, em variáveis próprias
    // Executa o script com a query, agora com dados passados e devolve o resultado da query ou erro.
    // HttpUrlConnection recebe a resposta, com os registos, através de con.getInputStream:
    //  - pelo canal de entrada inPutStreamReader
    //  - no cais da entrada BuferedRader
    // Os dados são empilhos na StringBuilder sb e devolvida para quem chamou o metodo
    //
    // O metodo tem 2 parametro: URL do script php e o nProc;
    public String sendGetRequest(String phpURL,String nProc){
        StringBuilder sb = new StringBuilder();
        try{
            URL url = new URL(phpURL+nProc);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            //Conversao da resposta do server, com os dados, para uma stringBuilder
            //lembrar que vem numa string, com formato de um array
            String strLine;

            while ((strLine = bufferedReader.readLine()) != null){
                sb.append(strLine+"\n");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            Log.i("LOG:", "WebConnection - sendGetRequest - Execucao: " + e.getMessage());
        }
        return sb.toString();
    }
    //Metodo auxiliar para Conversao dos dadosa enviar pelo sendPostRequest()
    private String convertData2Url(HashMap<String, String> dados) throws UnsupportedEncodingException{
        StringBuilder dadosConvertidos = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : dados.entrySet()){       //Foreach: Para cada par
            if (first){
                first = false;
            }
            else{
                dadosConvertidos.append("&");           //A partir do 1ºconcatena-os com &
            }

            //Convert numa string URL, em que os dados sao pares ligados por "=" e separados por &
            dadosConvertidos.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            dadosConvertidos.append("=");
            dadosConvertidos.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return dadosConvertidos.toString();
    }
}
