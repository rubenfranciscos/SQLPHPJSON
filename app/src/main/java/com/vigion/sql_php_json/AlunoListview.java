package com.vigion.sql_php_json;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AlunoListview extends AppCompatActivity {

    private ListView listView;          // Objeto javapara o controlo do objeto grafico ListView
    private String JSON_STRING;         //String para tratamento de dados JSON
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aluno_listview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View View, int position, long id) {
                //Obtêm o item com as duas string: o 1º é nProc, o 2º é name
                HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);

                //Extrai o nProc do item
                String itemNproc = map.get(Config.TAG_NPROC).toString();
                String itemNome = map.get(Config.TAG_NOME).toString();
                Toast.makeText(AlunoListview.this, itemNproc +"\n"+  itemNome, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(AlunoListview.this, AlunoEdit.class);
                intent.putExtra(Config.ALUNO_NPROC, itemNproc);
                startActivity(intent);
                //Prepara a chamada da activity para exibir os dados do item clicado
                //TODO: Por fazer
            }
        });

        //getAlunos();

        Log.i("LOG:","AlunoListView - OnCreate() - COMPLETA");


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAlunoAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();
                startActivity(new Intent(AlunoListview.this, AlunoAdd.class));
            }
        });
        FloatingActionButton Refresh = (FloatingActionButton) findViewById(R.id.fabAlunoRefresh);
        Refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();
                getAlunos();
                //startActivity(new Intent(AlunoListview.this, AlunoAdd.class));
            }
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // get Alunos() - Cria e executa a classe assincrona para executar o script php que recolhe todos os registos
    //////////////////////////////////////////////////////////////////////////////////////////////////
    private void getAlunos(){
        class GetAlunos extends AsyncTask<Void, Void, String>{
            ProgressDialog loading;     //Cria uma progressbar em caixa de dialogo

            @Override
            protected void onPreExecute() {
                loading = ProgressDialog.show(AlunoListview.this,"A processar rececao de daodos...", "So um momento...",false,false);
            }

            @Override
            protected String doInBackground(Void... params) {
                WebConnection wc =new WebConnection();
                Log.i("LOG:","AlunoListView - GetJSON - Chamar WebConnection.sendGetRequest" + Config.URL_GET_ALL);
                String response = wc.sendGetRequest(Config.URL_GET_ALL);
                Log.i("LOG:","AlunoListView - GetJSON - Resposta da WebConnection.senGetRequest" + response);
                return response;        //Envia a resposta para o metodo onPostExecute
            }

            @Override
            protected void onPostExecute(String response) {
                loading.dismiss();              //para a progress bar
                JSON_STRING = response;         //Inicializa a constante com os dados json do server
                showList();                     //metodo para Carregar a ListView, com os dados , agora a JSON_STRING
            }
        }
        GetAlunos ga = new GetAlunos();
        ga.execute();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // showList() Extrai os dados JSON devolvidos pelo metodo getAlunos() e constroi a ListView
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void showList(){
        JSONObject jsonObject = null;       //Processa os dados em formato JSON, devolvidas pelo Server php

        //Colection com (Coollection ValuePair) para os itens Aluno com nProc e nome
        ArrayList<HashMap<String,String>> listaAlunos = new ArrayList<HashMap<String, String>>();

        try{
            //Tratamento da resposta com dados, obtida em doInBackground e armazena na string constante JSON_STRING
            //Lembrar que o Server php devolveu os dados dos alunos encontrados na tabela no formato JSON
            Log.i("LOG:", "AlunoListView - showList() - JSON_STRING:  " + JSON_STRING);
            jsonObject = new JSONObject(JSON_STRING);
            Log.i("LOG:", "AlunoListView - showList() - JSONObject:  " + jsonObject);
            JSONArray registosJson = jsonObject.getJSONArray("result");
            Log.i("LOG:", "AlunoListView - showList() - result:  " + registosJson);

            for(int i = 0 ; i<registosJson.length();i++)
            {
                JSONObject json = registosJson.getJSONObject(i);
                String nProc = json.getString(Config.TAG_NPROC);
                String name = json.getString(Config.TAG_NOME);

                HashMap<String,String> aluno = new HashMap<>();
                aluno.put(Config.TAG_NPROC,nProc);
                aluno.put(Config.TAG_NOME,name);
                listaAlunos.add(aluno);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
            Log.i("LOG:","AlunoListView - showList() - Execuçao:  " + ex.getMessage());
        }

        //Construçao da listView
        ListAdapter adapter = new SimpleAdapter(
            AlunoListview.this,
            listaAlunos,
            R.layout.listview_item,
            new String[]{Config.TAG_NPROC,Config.TAG_NOME},
            new int[]{R.id.nProc, R.id.name});

        listView.setAdapter(adapter);

        }
}
