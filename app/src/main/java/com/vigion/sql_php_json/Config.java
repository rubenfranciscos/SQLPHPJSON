package com.vigion.sql_php_json;

/**
 * Created by ruben on 19/01/2016.
 */
public class Config {
    //Localizacao do server dos scripts
    //public static final String HOST_PHP_LOCATION = "http://31.170.160.103/escola/";
    public static final String HOST_PHP_LOCATION = "http://188.37.13.116/shared/David/";
    //Endereço dos scripts
    public static final String URL_ADD = HOST_PHP_LOCATION + "addAluno.php";
    public static final String URL_GET_ALL = HOST_PHP_LOCATION + "getAllAlunos.php";
    public static final String URL_GET_ALUNO = HOST_PHP_LOCATION + "getAluno.php?nProc=";
    public static final String URL_UPDATE_ALUNO = HOST_PHP_LOCATION + "updateAluno.php";
    public static final String URL_DELETE_ALUNO = HOST_PHP_LOCATION + "deleteAluno.php?nProc=";

    //Nomes dos atributos a enviar nos Registos, para os scripts php
    public static final String KEY_NPROC = "nProc";
    public static final String KEY_NOME = "Nome";
    public static final String KEY_USERNAME = "UserName";
    public static final String KEY_PASSWORD = "Password";

    //json Tags, a extair dos dados remetidos pelo server
    public static final String TAG_JSON_ARRAY = "result";
    public static final String TAG_NPROC = "nProc";
    public static final String TAG_NOME = "Nome";
    public static final String TAG_USERNAME = "UserName";
    public static final String TAG_PASSWORD = "Password";

    //indentificador do registo a passar no Intent entre as activities da listview e AlunoEdit
    public static final String ALUNO_NPROC = "nProc";
    public static final String ALUNO_NOME = "nome";
    //public static final String ALUNO_USERNAME = "";
}
