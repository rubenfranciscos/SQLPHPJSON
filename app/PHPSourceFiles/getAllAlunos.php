<?php			//Pesquisa por 1 registo para ser editado
	//Obtém os dados do Request da app Android , para a DML
	require_once('dbConnect.php'); //Executa a ligação à php php
	
	$sql = "SELECT * FROM ALuno";
	$r = mysqli_query($con,$sql);
	
	//Receber a resposta do Server, com o registo encontrado
	$result = array();
	while($row = mysqli_fetch_array($r)){
		array_push($result,array(
			 "nProc"=>$row['nProc'],
			 "Nome"=>$row['Nome']
		));
	}

	//Este array de nome result vai ser devolvido Inteirinho para o android, no formato JSON 
	//No android , o array result é extraido e oo registo extraido. O nome é muito importante.
	//Codifica o array no formato JSON e devolve-a (Echo) ao android
	echo json_encode(array('result'=>$result));
	mysqli_close($con);
		
?>